CC = gcc
LIBS = -lwiringPi
CFLAGS = -g -lm
OBJ = RObucket1.o tests.o

RObucket: $(OBJ)
	$(CC) -o $@ $^ $(CFLAGS) $(LIBS)
  
%.o:	%.c *.h
	$(CC) -c -o $@ $< $(CFLAGS) $(LIBS)

clean:
	-rm RObucket RObucket1.o tests.o $(LIBS)
