#include <wiringPi.h>
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include "tests.h"
#include "RObucket.h"

#define BUFFER 100

const int midSensor = 1;
const int activeSensor = 4;
const int inactiveSensor =  5;
const int solenoidPin = 6;

FILE *csv;

/* gets name of file from user
 * opens the file and adds the column names
 */
void initializeFile(){
  
  printf("File Name(suggested: mousename date): ");
  char fileName[BUFFER];
  fgets(fileName, BUFFER, stdin);
  char *file = strcat(fileName, ".CSV");
  csv = fopen(file, "w");
  fprintf(csv, "Program, Time, Sugar Deliveries, Active Pokes, Inactive Pokes, Well Entries");
  
}

/* prompts user to enter a number to select a test
 * 1 => fixed ratio one
 * 2 => fixed ratio five
 * 3 => progressive ratio
 */
int chooseTest(){
  char input[BUFFER];
  int test;
  while(test < 1 || test > 3){  
	printf("\n(FR1 = 1, FR5 = 2, PR = 3)\n");
	printf("Enter Test: ");
	fgets(input, BUFFER, stdin);
	test = atoi(input);
  }
  //could use a check
  return test;
}


/* initializes file for data
 * gets test selection from user
 * runs the test and outputs data to file
 */
int main(){
  initializeFile();
  int test = chooseTest();
  
  wiringPiSetup();
  pinMode(midSensor, INPUT);
  pinMode(activeSensor, INPUT);
  pinMode(inactiveSensor, INPUT);
  pinMode(solenoidPin, OUTPUT);
  
  if(test == 1){
	printf("Starting Fixed Ratio 1 test\n"); //do they want this feedback?
    fixedRatio(1);
  }
  else if(test == 2){
	printf("Starting Fixed Ratio 5 test\n");
    fixedRatio(5);
  }
  else {
	printf("Starting Progressive Ratio test\n");
    progressiveRatio();
  }

}
