/*
* compile: need -l wiringPi
*/


// #include <wiringPi.h>
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <time.h>
#include <math.h>
#include <unistd.h>

#define BUFFER 100
#define SUGARAMT 60000

const int midSensor = 23; // change to actual pin
const int activeSensor = 18; // change to actual pin
const int inactiveSensor =  17; //change to actual pin
const int solenoidPin = 10; //change to actual pin

FILE *csv;


void initializeFile(){
  printf("File Name(suggested: mousename date): ");
  char fileName[BUFFER];
  fgets(fileName, BUFFER, stdin);
  char *file = strcat(fileName, ".CSV");
  csv = fopen(file, "w");
  fprintf(csv, "Program, Time, Sugar Deliveries, Active Pokes, Inactive Pokes, Well Entries");
}

int chooseTest(){
  char input[BUFFER];
  int test;
  printf("\n(FR1 = 1, FR5 = 2, PR = 3)\n");
  printf("Enter Test: ");
  fgets(input, BUFFER, stdin);
  //error handling, make sure test is 1-3, check that is correct test?
  test = atoi(input);
  return test;
}



void addRecord(char prog, int rewardCount, int activeCount, int inactiveCount){
  //time in updateTime function?
  time_t tp;
  time(&tp);
  struct tm *mytime = localtime(&tp);
  char dateTime[BUFFER];
  sprintf(dateTime, "%d/%d %d:%d:%d\n", mytime->tm_mon, mytime->tm_mday, mytime->tm_hour, mytime->tm_min, mytime->tm_sec);

  fprintf(csv, "\n%c,%s,%d,%d,%d,0", prog, dateTime, rewardCount, activeCount, inactiveCount);
}

void deliverSucrose(){
  //HIGH and LOW taken from original sketch, may not be the same
  digitalWrite(solenoidPin, HIGH); //open solenoid
  usleep(SUGARAMT); //usleep uses microseconds and is older, use nanosleep?
  digitalWrite(solenoidPin, LOW); //close solenoid
}

void fixedRatio1(){
  int activeCount = 0;
  int inactiveCount = 0;
  // int rewardCount = 0;
  time_t timeStart;
  time_t timeRecord;
  time(&timeStart);
  time(&timeRecord);

  while(activeCount < 50 && (difftime(timeRecord, timeStart) < 3600)){
    if(digitalRead(activeSensor) == 0){
      deliverSucrose();
      activeCount++;
      addRecord('1', activeCount, activeCount, inactiveCount);
    }
    else if(digitalRead(inactiveSensor) == 0){
      inactiveCount++;
      addRecord('1', activeCount, activeCount, inactiveCount);
    }
    time(&timeRecord);
  }
}

void fixedRatio5(){
  int ratioSatisfied = 0;
  int activeCount = 0;
  int inactiveCount = 0;
  int rewardCount = 0;
  time_t timeStart;
  time_t timeRecord;
  time(&timeStart);
  time(&timeRecord);

  while(rewardCount < 50 && (difftime(timeRecord, timeStart) < 3600)){
    if(digitalRead(activeSensor) == 0){
      activeCount++;
      ratioSatisfied++;

      if(ratioSatisfied == 5){
        deliverSucrose();
        rewardCount++;
        ratioSatisfied = 0;
      }
      addRecord('5', rewardCount, activeCount, inactiveCount);
    }
    else if(digitalRead(inactiveSensor) == 0){
      inactiveCount++;
      addRecord('5', rewardCount, activeCount, inactiveCount);
    }

    time(&timeRecord);
  }
}


void progressiveRatio(){
  int rewardCount = 0;
  int activeCount = 0;
  int inactiveCount = 0;
  int ratioSatisfied = 0;
  int ratio;
  //initialize overall time, need more informative names
  time_t timeStart;
  time_t timeRecord;
  time_t timeElapsed;
  time(&timeStart);
  time(&timeRecord);
  time(&timeElapsed);

  while(difftime(timeRecord, timeStart) < 3600 && difftime(timeRecord, timeElapsed) < 600){
    ratio = round((5 * exp(.2 * (rewardCount+1))) - 5);
    if(digitalRead(activeCount) == 0){
      activeCount++;
      ratioSatisfied++;
      if(ratioSatisfied == ratio){
        deliverSucrose();
        rewardCount++;
        ratioSatisfied = 0;
        //update to determine difference
        time(&timeElapsed);
      }
      addRecord('P', rewardCount, activeCount, inactiveCount);
    }
    if(digitalRead(inactiveCount) == 0){
      inactiveCount++;
      addRecord('P', rewardCount, activeCount, inactiveCount); //may not need this
    }
    time(&timeRecord);
  }
}

int main(){
  initializeFile();
  int test = chooseTest();
  /*taken out for compiling purposes*/
  // wiringPiSetupGPIO();
  //
  // pinMode(midSensor, INPUT);
  // pinMode(activeSensor, INPUT);
  // pinMode(inactiveSensor, INPUT);

  if(test == 1){
    fixedRatio1();
  }
  if(test == 2){
    fixedRatio5();
  }
  else {
    progressiveRatio();
  }


}
