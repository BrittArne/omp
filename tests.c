#include <wiringPi.h>
#include <time.h>
#include <math.h>
#include <string.h>
#include <stdlib.h>
#include <stdio.h> 
#include "tests.h"
#include "RObucket.h"


#define BUFFER 100
#define SUGARAMT 60
#define TESTTIMEOUT 3600 //should be 3600
#define REWARDTIMEOUT 600
#define MAXREWARD 50

/* gets time of nose poke (month/day hour:min:sec)
 * value is stored in dateTime
 */
char* updateTime(char dateTime[]){
  
  time_t tp;
  time(&tp);
  struct tm *mytime = localtime(&tp);
  sprintf(dateTime, "%d/%d %d:%d:%d", mytime->tm_mon + 1, mytime->tm_mday, mytime->tm_hour, mytime->tm_min, mytime->tm_sec);
}


/* writes which test, time of poke, current number of rewards dispensed,
 * current number of active hole pokes, and current number of inactive pokes
 * to file 
 */
void addRecord(char prog, int rewardCount, int activeCount, int inactiveCount, int wellEntries){
  
  char dateTime[BUFFER];
  updateTime(dateTime);
  
  fprintf(csv, "\n%c, %s, %d, %d, %d, %d", prog, dateTime, rewardCount, activeCount, inactiveCount, wellEntries);
 
  //for testing purposes
  printf("%c, %s, %d, %d, %d, %d\n", prog, dateTime, rewardCount, activeCount, inactiveCount, wellEntries);
}

/* sends signal to solenoid to dispense sugar
 * dispenses for SUGARAMT(60 ms) of time
 */
void deliverSucrose(){
  digitalWrite(solenoidPin, HIGH); //open solenoid
  delay(SUGARAMT); //could also use sleep, delay from original sketches
  digitalWrite(solenoidPin, LOW); //close solenoid
}

/* returns true if the active hole was poked */
int isActivePoke(){
  return (digitalRead(activeSensor) == 0);
}

/* returns true if the inactive hole was poked */
int isInactivePoke(){
  return (digitalRead(inactiveSensor) == 0);
}

int isWellPoke(){
	return (digitalRead(midSensor) == 0);
}

/* converts integer to char value*/
char intToChar(int value){
  return(value + '0');
}

/* runs a fixed ratio test while mouse has not received maximum number of rewards
 * and has not exceeded the timeout from the start of the test
 *
 * the ratio is equal to either 1 or 5 depending on user input
 *
 * if a hole is poked, the poke count for that sensor is incremented
 *
 * if the active hole was poked, the current active streak is incremented
 * when the current streak equals the ratio, a treat is dispensed and the 
 * current streak is reset
 * 
 * record of current test, reward count, active and inactive hole poke count
 * is written out to file
 *
 */
void fixedRatio(int ratio){
  
  int currentStreak = 0;
  int activeCount = 0;
  int inactiveCount = 0;
  int rewardCount = 0;
  int wellEntries = 0;
  
  time_t startTime;
  time_t totalTime;
  time(&startTime);
  time(&totalTime);
  //remove "rewardCount < MAXREWARD &&" to only test on time and not reward
  while(rewardCount < MAXREWARD && (difftime(totalTime, startTime) < TESTTIMEOUT)){
	  
    if(isActivePoke()){
      activeCount++;
      currentStreak++;
	  
      if(currentStreak == ratio){
        deliverSucrose();
        rewardCount++;
        currentStreak = 0;
      }
      addRecord(intToChar(ratio), rewardCount, activeCount, inactiveCount, wellEntries);
      delay(50); //delay before next nosepoke// set to millisecond use nanosleep?
    }
    else if(isInactivePoke()){
	  //currentStreak = 0; will reset streak after an inactive nose poke
      inactiveCount++;
      addRecord(intToChar(ratio), rewardCount, activeCount, inactiveCount, wellEntries);
      delay(50); //delay before next nosepoke, set for 1 millisecond
    }else if(isWellPoke()){
		wellEntries++;
		addRecord(intToChar(ratio), rewardCount, activeCount, inactiveCount, wellEntries);
		delay(50);
	}
    time(&totalTime);
  }
  
}

/* runs a progressive ratio test while mouse has not exceeded timeout for the test
 * and timeout between rewards
 *
 * ratio depends on how many rewards the mouse has received so far
 *
 * if a hole is poked, the poke count for that sensor is incremented
 *
 * if the active hole was poked, the current active streak is incremented
 * when the current streak equals the ratio, a treat is dispensed, and the
 * current streak and reward timeout are reset
 * 
 * record of current test, reward count, active and inactive hole poke count
 * is written out to file
 *
 */
void progressiveRatio(){
  int rewardCount = 0;
  int activeCount = 0;
  int inactiveCount = 0;
  int currentStreak = 0;
  int wellEntries = 0;
  int ratio;
  //initialize overall time
  time_t startTime;
  time_t totalTime;
  time_t lastRewardTime;
  time(&startTime);
  time(&totalTime);
  time(&lastRewardTime);

  while(difftime(totalTime, startTime) < TESTTIMEOUT && difftime(totalTime, lastRewardTime) < REWARDTIMEOUT){
    
    int ratio = (int)(((5 * exp(.2 * (rewardCount+1))) - 5) + .5);
    
    if(isActivePoke()){
      activeCount++;
      currentStreak++;
      if(currentStreak == ratio){
        deliverSucrose();
        rewardCount++;
        currentStreak = 0;
        //update to determine difference between total time and last reward
        time(&lastRewardTime);
      }
      addRecord('P', rewardCount, activeCount, inactiveCount, wellEntries);
      delay(50);
    }
    if(isInactivePoke()){
  	  //currentStreak = 0; will reset streak after an inactive nose poke
      inactiveCount++;
      addRecord('P', rewardCount, activeCount, inactiveCount, wellEntries);
      delay(50); //delay after nose poke, set for one millisecond
    }
    if(isWellPoke()){
		wellEntries++;
		addRecord('P', rewardCount, activeCount, inactiveCount, wellEntries);
		delay(50);
	}
    time(&totalTime);
  }
}
